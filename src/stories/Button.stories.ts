// also exported from '@storybook/angular' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/angular/types-6-0';
import Button from './button.component';

export default {
  title: 'Example/Button',
  component: Button,
  argTypes: {
    backgroundColor: { control: 'color' },
  },
} as Meta;

const Template: Story<Button> = (args: Button) => ({
  component: Button,
  props: args,
});

export const Primary = Template.bind({});
Primary.args = {
  primary: true,
  label: 'Primary Button',
};

export const PrimaryWithIcon = Template.bind({});
PrimaryWithIcon.args = {
  ...Primary.args,
  label: 'Primary With Icon Button',
  withIcon : true
};

export const Secondary = Template.bind({});
Secondary.args = {
  label: 'Secondary Button',
};

export const SecondaryWithIcon = Template.bind({});
SecondaryWithIcon.args = {
  ...Secondary.args,
  label: 'Secondary With Icon Button',
  withIcon : true
};

export const Link = Template.bind({});
Link.args = {
  link: true,
  label: 'Link Button',
};

export const LinkWithIcon = Template.bind({});
LinkWithIcon.args = {
  ...Link.args,
  label: 'Link With Icon Button',
  withIcon : true
};

export const Large = Template.bind({});
Large.args = {
  size: 'large',
  label: 'Large Button',
};

export const Small = Template.bind({});
Small.args = {
  size: 'small',
  label: 'Small Button',
};
